#include <iostream>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include "card.h"

using namespace std;

const int size_ = 2;
int sum[size_]{ 0,0 };
int flag_of_error = 1;

int input(string volume) {
	int input_number;
	cout << "Please input "<<volume<<": ";
	cin >> input_number;
	cout << "\n";
	return input_number;
}

void degree(int degre, int number) {
	int num = number;
	for (int i = 1; i < degre; i++)
	{
		num *= number;
	}
	char ch(94);
	cout << "Result number" << ch << "degree: " << num << endl << endl;
}

void task1() {
	cout << "Degree of number. \n\n";
	degree(input("degre"), input("number"));
}

void range(int to_, int from_) {
	int res = 0;
	for (int  i = from_+1; i < to_; i++)
	{
		res += i;
	}
	cout << "Result sum of ordinal numbers within the range (A..B): " << res << endl << endl;
	//return res;
}

void task2() {
	cout << "Sum of range (A..B).";
	range(input("finish range"), input("start range"));
}


/*
6
28
496
8128                         everything else takes too long
33 550 336
8 589 869 056
137 438 691 328
2 305 843 008 139 952 128
2 658 455 991 569 831 744 654 692 615 953 842 176
191 561 942 608 236 107 294 793 378 084 303 638 130 997 321 548 169 216
*/

double sum_of_divisors(int number) {
	double result = 0;
	for (int  i = 1; i < number; i++){
		if (!(number % i)) {
			result += i;
		}
	}
	return result;
}

void perfect_number(int number) {
	if (number == sum_of_divisors(number)) {
		cout << "We found the perfect number!\nThe perfect number is: " << number << endl;
	}
}

void find_perfect_number_in_range(int to_, int from_) {
	int res = 0;
	for (int i = from_; i <= to_; i++)
	{
		perfect_number(i);
	}
	cout << endl;
}

void task3() {
	cout << "Perfect number. \n\n";
	find_perfect_number_in_range(input("finish range"), input("start range"));
}

void task4() {
	cout << "Playing card. \n\n";
	card();
}

void sum_part_of_number(int number) {
	if((number>=100000) && (number < 1000000) ){
		for (int i = 0; i < 3; i++){
			sum[0] += number % 10;
			number /= 10;
		}
		for (int i = 0; i < 3; i++) {
			sum[1] += number % 10;
			number /= 10;
		}
	}
	else {
		cout << "Error: The entered number is not six-digit";
		flag_of_error = -1;
	}
}

void lucky_number(int number) {
	sum_part_of_number(number);
	if (sum[0] == sum[1]) {
		if (flag_of_error > 0)	cout << "\nThe number " << number << " is lucky number!\n";
	}
	else {
		if (flag_of_error > 0) cout << "\nThis number " << number << " is not a lucky number!\n";
	}
}

void task5() {
	cout << "Lucky number. \n\n";
	lucky_number(input("six-digit number"));
}

int main()
{
	int t;
	do
	{
		cout << "\n\nenter # task(to exit enter negative value):  ";
		cin >> t;
		switch (t)
		{
		case 1:
			task1();
			break;
		case 2:
			task2();
			break;
		case 3:
			task3();
			break;
		case 4:
			task4();
			break;
		case 5:
			task5();
			break;
		default:
			break;
		}
	} while (t >= 0);

	return 0;
}